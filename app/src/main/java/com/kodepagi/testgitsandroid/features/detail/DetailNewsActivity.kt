package com.kodepagi.testgitsandroid.features.detail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import com.facebook.drawee.view.SimpleDraweeView
import com.google.gson.Gson
import com.kodepagi.testgitsandroid.R
import com.kodepagi.testgitsandroid.data.model.Article
import com.kodepagi.testgitsandroid.features.base.BaseActivity
import com.kodepagi.testgitsandroid.util.Common

/**
 * Created by nur on 10/07/18.
 */
class DetailNewsActivity : BaseActivity() {

    @BindView(R.id.toolbar) @JvmField var mToolbar: Toolbar? = null
    @BindView(R.id.txt_title_news) @JvmField var mTxtTitleNews: TextView? = null
    @BindView(R.id.img_news) @JvmField var mImgNews: SimpleDraweeView? = null
    @BindView(R.id.txt_desc) @JvmField var mTxtDesc: TextView? = null
    @BindView(R.id.txt_source_news) @JvmField var mTxtSource: TextView? = null
    @BindView(R.id.txt_link) @JvmField var mTxtLink: TextView? = null
    @BindView(R.id.txt_author) @JvmField var mTxtAuthor: TextView? = null
    @BindView(R.id.txt_date) @JvmField var mTxtDate: TextView? = null

    override val layout: Int
    get() = R.layout.activity_detail_news

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(mToolbar)

        getExtras()
    }

    fun getExtras() {
        try {
            // get the Intent that started this Activity
            val `in` = intent
            // get the Bundle that stores the data of this Activity
            val dataBundle = `in`.extras
            if (dataBundle != null) {
                val extras = dataBundle.getString(EXTRA_ARTICLE)
                val news = Gson().fromJson(extras, Article::class.java)
                fillDetail(news)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun fillDetail(news: Article) {
        mTxtTitleNews?.setText(news.title)
        mTxtDesc?.setText(news.description)
        mTxtSource?.setText(news.source?.name)
        mTxtAuthor?.setText(this?.resources.getString(R.string.label_author, news.author))
        mTxtDate?.setText(Common.convertDate(news.publishedAt.toString(), "yyyy-MM-dd'T'HH:mm:ss","dd MMM yyyy, HH:mm"))
        mImgNews?.setImageURI(news.urlToImage)
        mTxtLink?.setText(news.url)
    }

    @OnClick(R.id.txt_link)
    protected fun onClickLink(){
        openUrl()
    }

    @OnClick(R.id.txt_source_news)
    protected fun onClickSource(){
        openUrl()
    }

    private fun openUrl() {
        val url = mTxtLink?.text.toString()
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }

    companion object {

        val EXTRA_ARTICLE = "EXTRA_ARTICLE"

        fun getStartIntent(context: Context, news: Article): Intent {
            val intent = Intent(context, DetailNewsActivity::class.java)
            intent.putExtra(EXTRA_ARTICLE, Gson().toJson(news))
            return intent
        }
    }

}