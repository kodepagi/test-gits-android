package com.kodepagi.practicekotlin2.features.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.facebook.drawee.view.SimpleDraweeView
import com.kodepagi.testgitsandroid.R
import com.kodepagi.testgitsandroid.data.model.Article
import com.kodepagi.testgitsandroid.util.Common
import com.kodepagi.testgitsandroid.injection.ApplicationContext
import javax.inject.Inject

class NewsAdapter @Inject
constructor(@ApplicationContext context: Context) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    private var mNews: List<Article>? = null
    private var mClickListener: ClickListener? = null
    var mContext: Context? = null

    init {
        mContext = context
        mNews = emptyList<Article>()
    }

    fun setNews(news: List<Article>) {
        mNews = news
    }

    fun setClickListener(clickListener: ClickListener) {
        mClickListener = clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_new, parent, false)
        return NewsViewHolder(view)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val news = mNews?.get(position)
        holder.onBind(news!!)
    }

    override fun getItemCount(): Int {
        return mNews?.size as Int
    }

    interface ClickListener {
        fun onNewsClick(news: Article)
    }

    open inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mNews: Article? = null
        @BindView(R.id.img_news) @JvmField var mImgNews: SimpleDraweeView? = null
        @BindView(R.id.txt_title_news) @JvmField var mTxtTitleNews: TextView? = null
        @BindView(R.id.txt_content_news) @JvmField var mTxtContentNews: TextView? = null
        @BindView(R.id.txt_source_news) @JvmField var mTxtSourceNews: TextView? = null
        @BindView(R.id.txt_author) @JvmField var mTxtAuthorNews: TextView? = null
        @BindView(R.id.txt_date) @JvmField var mTxtDateNews: TextView? = null

        init {
            ButterKnife.bind(this, itemView)
            itemView.setOnClickListener { if (mClickListener != null) mClickListener?.onNewsClick(mNews as Article) }
        }

        internal fun onBind(news: Article) {
            mNews = news
            mTxtTitleNews?.setText(news.title)
            mTxtContentNews?.setText(news.description)
            mTxtSourceNews?.setText(news.source?.name)
            mTxtAuthorNews?.setText(mContext?.resources?.getString(R.string.label_author, news.author))
            mTxtDateNews?.setText(Common.convertDate(news.publishedAt.toString(), "yyyy-MM-dd'T'HH:mm:ss","dd MMM yyyy, HH:mm"))
            mImgNews?.setImageURI(news.urlToImage)
        }
    }
}