package com.kodepagi.testgitsandroid.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by nur on 10/07/18.
 */
class Article {

    @SerializedName("source")
    var source: Source? = null
    @SerializedName("author")
    var author: String? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("url")
    var url: String? = null
    @SerializedName("urlToImage")
    var urlToImage: String? = null
    @SerializedName("publishedAt")
    var publishedAt: String? = null

    constructor(source: Source?, author: String?, title: String?, description: String?, url: String?, urlToImage: String?, publishedAt: String?) {
        this.source = source
        this.author = author
        this.title = title
        this.description = description
        this.url = url
        this.urlToImage = urlToImage
        this.publishedAt = publishedAt
    }

}