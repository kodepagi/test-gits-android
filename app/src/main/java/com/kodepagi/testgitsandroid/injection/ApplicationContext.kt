package com.kodepagi.testgitsandroid.injection


import javax.inject.Qualifier

@Qualifier @Retention annotation class ApplicationContext
