package com.kodepagi.testgitsandroid.data

import com.kodepagi.testgitsandroid.data.model.NewsResponse
import com.kodepagi.testgitsandroid.data.model.Pokemon
import com.kodepagi.testgitsandroid.data.remote.PokemonApi
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataManager @Inject
constructor(private val mPokemonApi: PokemonApi) {

    fun getPokemonList(limit: Int): Single<List<String>> {
        return mPokemonApi.getPokemonList(limit)
                .toObservable()
                .flatMapIterable { (results) -> results }
                .map { (name) -> name }
                .toList()
    }

    fun getPokemon(name: String): Single<Pokemon> {
        return mPokemonApi.getPokemon(name)
    }

    fun getTopHeadlines(): Single<NewsResponse> {
        return mPokemonApi.getTopHeadlines("id", "ca9afa7acdb2485b8d61ee36534b80da")
    }

}