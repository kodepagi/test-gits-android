package com.kodepagi.testgitsandroid.data.model

data class NamedResource(var name: String, var url: String)
