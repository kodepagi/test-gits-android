package com.kodepagi.testgitsandroid.injection.component

import android.app.Application
import android.content.Context
import dagger.Component
import com.kodepagi.testgitsandroid.data.DataManager
import com.kodepagi.testgitsandroid.data.remote.PokemonApi
import com.kodepagi.testgitsandroid.injection.ApplicationContext
import com.kodepagi.testgitsandroid.injection.module.AppModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    @ApplicationContext
    fun context(): Context

    fun application(): Application

    fun dataManager(): DataManager

    fun pokemonApi(): PokemonApi
}
