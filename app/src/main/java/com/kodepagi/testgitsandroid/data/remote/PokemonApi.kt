package com.kodepagi.testgitsandroid.data.remote


import com.kodepagi.testgitsandroid.data.model.NewsResponse
import com.kodepagi.testgitsandroid.data.model.Pokemon
import com.kodepagi.testgitsandroid.data.model.PokemonListResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonApi {

    @GET("pokemon")
    fun getPokemonList(@Query("limit") limit: Int): Single<PokemonListResponse>

    @GET("pokemon/{name}")
    fun getPokemon(@Path("name") name: String): Single<Pokemon>

    @GET("top-headlines")
    fun getTopHeadlines(@Query("country") coutry: String, @Query("apiKey") token: String): Single<NewsResponse>

}
