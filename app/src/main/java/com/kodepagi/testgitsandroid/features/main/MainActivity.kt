package com.kodepagi.testgitsandroid.features.main

import com.kodepagi.testgitsandroid.R
import com.kodepagi.testgitsandroid.features.base.BaseActivity
import com.kodepagi.testgitsandroid.features.common.ErrorView
import com.kodepagi.testgitsandroid.features.detail.DetailActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ProgressBar
import butterknife.BindView
import com.kodepagi.practicekotlin2.features.main.NewsAdapter
import com.kodepagi.testgitsandroid.data.model.Article
import com.kodepagi.testgitsandroid.data.model.NewsResponse
import com.kodepagi.testgitsandroid.features.detail.DetailNewsActivity
import timber.log.Timber
import javax.inject.Inject

class MainActivity : BaseActivity(), MainMvpView, NewsAdapter.ClickListener, ErrorView.ErrorListener {

    @Inject lateinit var mNewsAdapter: NewsAdapter
    @Inject lateinit var mMainPresenter: MainPresenter

    @BindView(R.id.view_error) @JvmField var mErrorView: ErrorView? = null
    @BindView(R.id.progress) @JvmField var mProgress: ProgressBar? = null
    @BindView(R.id.recycler_pokemon) @JvmField var mNewsRecycler: RecyclerView? = null
    @BindView(R.id.swipe_to_refresh) @JvmField var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    @BindView(R.id.toolbar) @JvmField var mToolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent().inject(this)
        mMainPresenter.attachView(this)

        setSupportActionBar(mToolbar)

        mSwipeRefreshLayout?.setProgressBackgroundColorSchemeResource(R.color.primary)
        mSwipeRefreshLayout?.setColorSchemeResources(R.color.white)
        mSwipeRefreshLayout?.setOnRefreshListener { mMainPresenter.getTopHeadlines() }

        mNewsAdapter.setClickListener(this)
        mNewsRecycler?.layoutManager = LinearLayoutManager(this)
        mNewsRecycler?.adapter = mNewsAdapter

        mErrorView?.setErrorListener(this)

        mMainPresenter.getTopHeadlines()
    }

    override val layout: Int
        get() = R.layout.activity_main

    override fun onDestroy() {
        super.onDestroy()
        mMainPresenter.detachView()
    }

    override fun showPokemon(pokemon: List<String>) {

    }

    override fun getTopHeadlineSuccess(newResponse: NewsResponse) {
        mNewsAdapter.setNews(newResponse.articles!!)
        mNewsAdapter.notifyDataSetChanged()

        mNewsRecycler?.visibility = View.VISIBLE
        mSwipeRefreshLayout?.visibility = View.VISIBLE
    }

    override fun showProgress(show: Boolean) {
        if (show) {
            if (mNewsRecycler?.visibility == View.VISIBLE && mNewsAdapter.itemCount > 0) {
                mSwipeRefreshLayout?.isRefreshing = true
            } else {
                mProgress?.visibility = View.VISIBLE

                mNewsRecycler?.visibility = View.GONE
                mSwipeRefreshLayout?.visibility = View.GONE
            }

            mErrorView?.visibility = View.GONE
        } else {
            mSwipeRefreshLayout?.isRefreshing = false
            mProgress?.visibility = View.GONE
        }
    }

    override fun showError(error: Throwable) {
        mNewsRecycler?.visibility = View.GONE
        mSwipeRefreshLayout?.visibility = View.GONE
        mErrorView?.visibility = View.VISIBLE
        Timber.e(error, "There was an error retrieving the pokemon")
    }

    override fun onNewsClick(news: Article) {
        startActivity(DetailNewsActivity.getStartIntent(this, news))
        overridePendingTransition(R.anim.anim_enter_fade, R.anim.anim_exit_fade)
    }

    override fun onReloadData() {
        mMainPresenter.getTopHeadlines()
    }

    companion object {

        private val POKEMON_COUNT = 20
    }
}