package com.kodepagi.testgitsandroid.injection.component

import com.kodepagi.testgitsandroid.injection.PerActivity
import com.kodepagi.testgitsandroid.injection.module.ActivityModule
import com.kodepagi.testgitsandroid.features.base.BaseActivity
import com.kodepagi.testgitsandroid.features.detail.DetailActivity
import com.kodepagi.testgitsandroid.features.main.MainActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    fun inject(baseActivity: BaseActivity)

    fun inject(mainActivity: MainActivity)

    fun inject(detailActivity: DetailActivity)
}
