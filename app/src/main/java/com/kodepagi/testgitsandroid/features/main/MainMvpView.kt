package com.kodepagi.testgitsandroid.features.main

import com.kodepagi.testgitsandroid.data.model.NewsResponse
import com.kodepagi.testgitsandroid.features.base.MvpView

interface MainMvpView : MvpView {

    fun showPokemon(pokemon: List<String>)

    fun getTopHeadlineSuccess(newResponse: NewsResponse)

    fun showProgress(show: Boolean)

    fun showError(error: Throwable)

}