package com.kodepagi.testgitsandroid.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by nur on 10/07/18.
 */
class Source {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null

    constructor(id: String?, name: String?) {
        this.id = id
        this.name = name
    }

}