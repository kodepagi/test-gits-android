package com.kodepagi.testgitsandroid.injection.component

import com.kodepagi.testgitsandroid.injection.PerFragment
import com.kodepagi.testgitsandroid.injection.module.FragmentModule
import dagger.Subcomponent

/**
 * This component inject dependencies to all Fragments across the application
 */
@PerFragment
@Subcomponent(modules = arrayOf(FragmentModule::class))
interface FragmentComponent