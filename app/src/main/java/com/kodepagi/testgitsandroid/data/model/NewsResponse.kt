package com.kodepagi.testgitsandroid.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by nur on 10/07/18.
 */
class NewsResponse {

    @SerializedName("status")
    var status: String? = null
    @SerializedName("totalResults")
    var totalResults: Int? = null
    @SerializedName("articles")
    var articles: List<Article>? = null

    constructor(status: String?, totalResults: Int?, articles: List<Article>?) {
        this.status = status
        this.totalResults = totalResults
        this.articles = articles
    }

}