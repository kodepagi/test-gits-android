package com.kodepagi.testgitsandroid.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by nur on 10/07/18.
 */
object Common {

    val locale = Locale("en")

    fun convertDate(inputString : String, inputPattern : String ,outputPattern : String) : String{
        val inputFormat = SimpleDateFormat(inputPattern, locale)
        val outputformat = SimpleDateFormat(outputPattern, locale)
        var strOutput = ""

        var date: Date?

        try {
            date = inputFormat.parse(inputString)
            strOutput = outputformat.format(date)
        }catch (e : ParseException){
            e.printStackTrace();
        }

        return strOutput
    }

}