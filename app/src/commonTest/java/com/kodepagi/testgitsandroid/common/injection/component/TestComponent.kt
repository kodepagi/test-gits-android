package com.kodepagi.testgitsandroid.common.injection.component

import dagger.Component
import com.kodepagi.testgitsandroid.common.injection.module.ApplicationTestModule
import com.kodepagi.testgitsandroid.injection.component.AppComponent
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationTestModule::class))
interface TestComponent : AppComponent