package com.kodepagi.testgitsandroid.features.detail

import com.kodepagi.testgitsandroid.data.model.Pokemon
import com.kodepagi.testgitsandroid.data.model.Statistic
import com.kodepagi.testgitsandroid.features.base.MvpView

interface DetailMvpView : MvpView {

    fun showPokemon(pokemon: Pokemon)

    fun showStat(statistic: Statistic)

    fun showProgress(show: Boolean)

    fun showError(error: Throwable)

}