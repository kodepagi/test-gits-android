package com.kodepagi.testgitsandroid.features.main

import com.kodepagi.testgitsandroid.data.DataManager
import com.kodepagi.testgitsandroid.injection.ConfigPersistent
import com.kodepagi.testgitsandroid.features.base.BasePresenter
import com.kodepagi.testgitsandroid.util.rx.scheduler.SchedulerUtils
import javax.inject.Inject

@ConfigPersistent
class MainPresenter @Inject
constructor(private val mDataManager: DataManager) : BasePresenter<MainMvpView>() {

    override fun attachView(mvpView: MainMvpView) {
        super.attachView(mvpView)
    }

    fun getPokemon(limit: Int) {
        checkViewAttached()
        mvpView?.showProgress(true)
        mDataManager.getPokemonList(limit)
                .compose(SchedulerUtils.ioToMain<List<String>>())
                .subscribe({ pokemons ->
                    mvpView?.showProgress(false)
                    mvpView?.showPokemon(pokemons)
                }) { throwable ->
                    mvpView?.showProgress(false)
                    mvpView?.showError(throwable)
                }
    }

    fun getTopHeadlines() {
        checkViewAttached()
        mvpView?.showProgress(true)
        mDataManager.getTopHeadlines()
                .compose(SchedulerUtils.ioToMain())
                .subscribe({ newResponse ->
                    mvpView?.showProgress(false)
                    mvpView?.getTopHeadlineSuccess(newResponse)
                }) { throwable ->
                    mvpView?.showProgress(false)
                    mvpView?.showError(throwable)
                }
    }

}