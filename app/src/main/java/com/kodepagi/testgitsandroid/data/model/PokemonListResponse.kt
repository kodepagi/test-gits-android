package com.kodepagi.testgitsandroid.data.model

data class PokemonListResponse(val results: List<NamedResource>)

